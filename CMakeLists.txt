# Run cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_ECLIPSE_GENERATE_SOURCE_PROJECT=TRUE -DCMAKE_ECLIPSE_VERSION=4.2 -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j8 ..
# from "build" folder to create eclipse project files and makefiles

# The name of our project is "NEURAL". CMakeLists files in this project can 
# refer to the root source directory of the project as ${NEURAL_SOURCE_DIR} and 
# to the root binary directory of the project as ${NEURAL_BINARY_DIR}. 
cmake_minimum_required (VERSION 2.8) 
project (NEURAL)
set (CMAKE_VERBOSE_MAKEFILE ON)

# Recurse into the "src", "doc" and "test" subdirectories. This does not actually 
# cause another cmake executable to run. The same process will walk through 
# the project's entire directory structure. 
add_subdirectory (src) 
add_subdirectory (test)
#add_subdirectory (doc)









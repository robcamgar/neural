/*
 * NetworkTest.h
 *
 *  Created on: 20/01/2013
 *  Author: Roberto de la C�mara
 */

#include <gtest/gtest.h>
#include "../src/Network.h"
#include <ctime>


using namespace network;

// The fixture class for Network testing
class NetworkTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if its body
  // is empty.
  int networkSize;
  int iterations;

  NetworkTest() {
    // You can do set-up work for each test here.
    iterations=0;
    networkSize=0;
  }

  virtual ~NetworkTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    // Code here will be called immediately after the constructor (right
    // before each test).
	networkSize=9;
	iterations=100;
	::testing::FLAGS_gtest_death_test_style = "threadsafe";
    srand((unsigned)time(0));

  }

  virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
  }

  virtual int delta(int i, int j) {
	  if(i==j)
		  return 1;
	  else
		  return 0;
  }


};


/*
 * Hopfield_Test.cpp
 *
 *  Created on: 17/01/2013
 *      Author: Roberto de la C�mara
 */


#include <gtest/gtest.h>

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}



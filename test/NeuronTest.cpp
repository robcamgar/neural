/*
 * NeuronTest.cpp
 *
 *  Created on: 17/01/2013
 *      Author: Roberto de la Camara
 */

#include "NeuronTest.h"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <armadillo>

using namespace neuron;


TEST_F(NeuronTest, create) {

  Neuron* neuron = 0;
  neuron = new Neuron();
  ASSERT_TRUE(neuron != 0);
  delete neuron;

}

TEST_F(NeuronTest, update) {

  Neuron neuron;

  arma::vec weights(10);
  arma::uvec activationStates(10);

  typedef boost::mt19937                     ENG;    // Mersenne Twister
  typedef boost::normal_distribution<double> DIST;   // Normal Distribution
  typedef boost::variate_generator<ENG,DIST> GEN;    // Variate generator

  ENG  eng;
  DIST dist(-10,10);
  GEN  gen(eng,dist);

  for (int i = 0; i < 10; i++){

	 weights[i]=gen();
	 if(weights[i]>0)
		 activationStates[i]=1;
	 else
		 activationStates[i]=-1;

  }

  neuron.update(weights, activationStates);

  ASSERT_TRUE(neuron.getState()==1 || neuron.getState()==-1);

}

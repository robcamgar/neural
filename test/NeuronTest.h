/*
 * NeuronTest.h
 *
 *  Created on: 17/01/2013
 *      Author: roberto
 */

#include <gtest/gtest.h>
#include "../src/Neuron.h"


using namespace neuron;

// The fixture class for Neuron testing
class NeuronTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if its body
  // is empty.

  NeuronTest() {
    // You can do set-up work for each test here.

  }

  virtual ~NeuronTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    // Code here will be called immediately after the constructor (right
    // before each test).

  }

  virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
  }

};

/*
 * NetworkTest.cpp
 *
 *  Created on: 20/01/2013
 *  Author: Roberto de la C�mara
 */

#include "NetworkTest.h"
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/symmetric.hpp>
#include <boost/shared_ptr.hpp>
#include <cstdio>

using namespace arma;


TEST_F(NetworkTest, create) {

  Network* network = 0L;
  network = new Network(10);
  ASSERT_TRUE(network != 0L);
  delete network;

}

TEST_F(NetworkTest, createTooSmall) {

  Network* network = 0L;

  ASSERT_DEATH(network = new Network(1),"");

  if(network!=0L)
	  delete network;
}


TEST_F(NetworkTest, update) {

	Network network(networkSize);
	arma::uvec neuronStates(networkSize), updatedNeuronStates(networkSize);
	arma::mat connectionMatrix=arma::zeros(networkSize,networkSize);

	//Random Numbers generation
	typedef boost::minstd_rand base_generator_type;
	base_generator_type generator(42u);

	// Define a uniform random number distribution which produces "double"
	// values between -10 and 10 (-10 inclusive, 10 exclusive).
	boost::uniform_real<> uni_dist(-10,10);
	boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni(generator, uni_dist);

	std::cout.setf(std::ios::fixed);
	// You can now retrieve random numbers from that distribution by means
	// of a STL Generator interface, i.e. calling the generator as a zero-
	// argument function.

	double randomNumber;
	for(int i = 0; i < networkSize; i++)
	{
	    randomNumber=uni();


	    if(randomNumber>0)
	    	neuronStates[i]=1;
	    else
	    	neuronStates[i]=-1;

        for (int j=0; j<networkSize; j++)
        {
        	if(i==j)
        		connectionMatrix(i,j)=0;
        	else
        	{
        	    randomNumber=uni();
        	    connectionMatrix(i,j)=connectionMatrix(j,i)=randomNumber;
        	}
        }

	}
    printf("UNO");

    network.loadInitialState(neuronStates);
    printf("TRES");
    network.loadConnectionMatrix(connectionMatrix);
    printf("CUATRO");
	network.update();
	printf("CINCO");
	updatedNeuronStates=network.getNeuronStates();



	int steps=0;
	std::clock_t start = std::clock();

    while(steps<iterations) {
		network.update();
		updatedNeuronStates=network.getNeuronStates();
		steps++;
	}


	bool networkConverges=true;
	vector<int> delta(networkSize);
	for (int i=0;i<networkSize;i++)
	{
		delta[i]=updatedNeuronStates[i]-neuronStates[i];
		if(delta[i]!=0)
		{
			networkConverges=false;
			break;
		}
	}
	std::clock_t stop = std::clock();

	printf("Iteration Elapsed Time: %.3lf seconds", (double)(stop-start)/CLOCKS_PER_SEC);

    ASSERT_FALSE(networkConverges);

}


TEST_F(NetworkTest, nonSquareMatrix) {

	mat connectionMatrix(networkSize,networkSize+1);
	for(int i=0;i<networkSize;i++)
		for(int j=0;j<networkSize+1;j++)
	        connectionMatrix(i,j)=1;

	Network network(networkSize);
	ASSERT_DEATH(network.loadConnectionMatrix(connectionMatrix),"");

}

TEST_F(NetworkTest, wrongMatrixSize) {

	mat connectionMatrix(networkSize+1,networkSize+1);
	for(int i=0;i<networkSize;i++)
		for(int j=0;j<networkSize+1;j++)
	        connectionMatrix(i,j)=1;

	Network network(networkSize);
	ASSERT_DEATH(network.loadConnectionMatrix(connectionMatrix),"");

}

/*
TEST_F(NetworkTest, associativeMemory) {

	Network network(networkSize);
	std::vector<int> neuronStates(networkSize), updatedNeuronStates(networkSize);
	boost::numeric::ublas::vector<int> pattern1(networkSize),pattern2(networkSize),pattern3(networkSize);
	mat A = zeros(networkSize, networkSize);
	mat connectionMatrix = symmatu(A);


	//1-Generate 3 random vectors to be memorized

	//Random Numbers generation
	typedef boost::minstd_rand base_generator_type;
	base_generator_type generator(42u);
	boost::uniform_int<> uni_dist(-1,1);
	boost::variate_generator<base_generator_type&, boost::uniform_int<> > uni(generator, uni_dist);
    std::cout.setf(std::ios::fixed);


	for (int i=0;i<networkSize;i++)
	{
		pattern1[i]=uni();
	    if(pattern1[i]==0)
	    	pattern1[i]=-1;
	}
	std::cout<<"\nInput pattern #1:"<<std::endl;
	std::cout<<pattern1<<std::endl;
	for (int i=0;i<networkSize;i++)
	{
	    pattern2[i]=uni();
	    if(pattern2[i]==0)
	    	pattern2[i]=-1;
	}
	std::cout<<"\nInput pattern #2:"<<std::endl;
	std::cout<<pattern2<<std::endl;
	for (int i=0;i<networkSize;i++)
	{
	    pattern3[i]=uni();
	    if(pattern3[i]==0)
	    	 pattern3[i]=-1;
	}
	std::cout<<"\nInput pattern #3:"<<std::endl;
	std::cout<<pattern3<<std::endl;


	 //2- Calculate connection weights
	 //    Wij=(1/N)SUMm(XjXk)m



	double partial1,partial2,partial3;
	double total;
	for(int i=0;i<networkSize;++i)
	{
		for(int j=0;j<i;++j)
		{
			partial1=(2*pattern1(i)-1)*(2*pattern1(j)-1);
			partial2=(2*pattern2(i)-1)*(2*pattern2(j)-1);
			partial3=(2*pattern3(i)-1)*(2*pattern3(j)-1);
			total=partial1+partial2+partial3;
			connectionMatrix(i,j)=total;

		}
	}


	 //3-Present a pattern to the network similar to pattern2



	for (int i=0;i<networkSize-1;i++)
	{
	     neuronStates[i]=pattern2(i);
	}
	neuronStates[networkSize-1]=-pattern2(networkSize-1);

	boost::numeric::ublas::vector<int> states(networkSize);
	for (int i=0;i<networkSize-1;i++)
	{
		    states[i]=neuronStates [i];
	}

	std::cout<<"\nPattern presented to the network:"<<std::endl;
	std::cout<<states<<std::endl;


	 //4-Load it and iterate until convergence is reached



	 network.loadInitialState(neuronStates);
	 arma::mat inputMatrix=connectionMatrix;
	 network.loadConnectionMatrix(inputMatrix);

	 int steps=0;
	 boost::numeric::ublas::vector<int> delta(networkSize);
	 std::clock_t start = std::clock();

	 network.update();
	 updatedNeuronStates=network.getNeuronStates();


	 bool more=true;
	 while(more&&steps<10000) {

		    network.update();
	 		updatedNeuronStates=network.getNeuronStates();

	        for (int i=0;i<networkSize;i++)
	        {
	 		    delta[i]=updatedNeuronStates[i]-neuronStates[i];

	        }
	        std::cout<<"\nDelta:"<<std::endl;
	       	std::cout<<delta<<std::endl;

	       	bool converged=true;
	       	for (int i=0;i<networkSize;i++)
	       		if(delta(i)!=0)
	       			converged=false;

	        neuronStates=updatedNeuronStates;
	        steps++;
	        more=!converged;

	 }

	 boost::numeric::ublas::vector<int> upStates(networkSize);
	 for (int i=0;i<networkSize-1;i++)
	 {
	 		    upStates[i]=updatedNeuronStates [i];
	 }

	 std::cout<<"\nOutput Pattern:"<<std::endl;
	 std::cout<<upStates<<std::endl;
	 printf("Convergence reached in %i steps",steps);
	 std::clock_t stop = std::clock();
     printf("Iteration Elapsed Time: %.3lf seconds\n", (double)(stop-start)/CLOCKS_PER_SEC);
     std::cout<<"\nOutput Pattern vs Input Pattern1:"<<std::endl;
     std::cout<<upStates-pattern1<<std::endl;
     std::cout<<"\nOutput Pattern vs Input Pattern2:"<<std::endl;
     std::cout<<upStates-pattern2<<std::endl;
     std::cout<<"\nOutput Pattern vs Input Pattern3:"<<std::endl;
     std::cout<<upStates-pattern3<<std::endl;

     ASSERT_FALSE(more);
}
*/
/*
TEST_F(NetworkTest, TSP) {

	unsigned int nodes=4;

	//1-Generate random symmetric Distance Matrix (0-100)



	symmetric_matrix<double,lower> connectionMatrix(nodes,nodes);

	for(unsigned int i=0;i<nodes;++i)
		for(unsigned int j=0;j<i;++j) {
			double alea=(double)rand()/((double)RAND_MAX/100);
			connectionMatrix(i,j)=alea;
		}

	 std::cout<<"\nConnection Matrix:"<<std::endl;
	 std::cout<<connectionMatrix<<std::endl;

     //2-Calculate weight matrix

     const unsigned int networkSize=nodes*nodes;
     matrix<double> weightMatrix(networkSize,networkSize);

     double A,B,C,D;
     A=1;
     B=1;
     C=0.4;
     D=1.5;

     for(unsigned int i=0;i<nodes;++i)
     		for(unsigned int j=0;j<nodes;++j)
     			for(unsigned int k=0;k<nodes;++k)
     			     for(unsigned int l=0;l<nodes;++l) {
     			    	 weightMatrix(i+l,j+k)=-A*delta(i,l)*(1-delta(k,j))-
     			    			 B*delta(k,j)*(1-delta(j,l))-
     			    			 C-
     			    			 D*connectionMatrix(j,l)*(delta(j,k+1)+delta(j,k-1));
     			     }
     std::cout<<"\nWeight Matrix:"<<std::endl;
     std::cout<<weightMatrix<<std::endl;

	 //3-Calculate random initial states

     vector<int> neuronStates(networkSize), updatedNeuronStates(networkSize);
     //Random Numbers generation
     typedef boost::minstd_rand base_generator_type;
     base_generator_type generator(42u);
     boost::uniform_int<> uni_dist(0,1);
     boost::variate_generator<base_generator_type&, boost::uniform_int<> > uni(generator, uni_dist);
     std::cout.setf(std::ios::fixed);
     for(unsigned int i=0;i<networkSize;++i)
    	 neuronStates(i)=uni();
     std::cout<<"\nInput Neuron States:"<<std::endl;
     std::cout<<neuronStates<<std::endl;

	 //4-Load Initial States and Connection Matrix


     shared_ptr<Network> network(new Network(networkSize));
     network->loadInitialState(neuronStates);
     network->loadConnectionMatrix(weightMatrix);


	 //5-Iterate calculating Energy function each step.

	 //   Iterate until two successive energy states are equal
	 //  in other words, until the output is stable


     int steps=0;

     vector<int> delta(networkSize);


     std::clock_t start = std::clock();


     network->update();
     updatedNeuronStates=network->getNeuronStates();


     bool more=true;
     while(more&&steps<100000) {

     		    network->update();
     	 		updatedNeuronStates=network->getNeuronStates();

     	        for (int i=0;i<networkSize;i++)
     	        {
     	 		    delta(i)=updatedNeuronStates(i)-neuronStates(i);

     	        }


     	       	bool converged=true;
     	       	for (int i=0;i<networkSize;i++)
     	       		if(delta(i)!=0)
     	       			converged=false;

     	        neuronStates=updatedNeuronStates;
     	        steps++;
     	        more=!converged;

     }

     std::clock_t stop = std::clock();

     printf("Iteration Elapsed Time: %.3lf seconds\n", (double)(stop-start)/CLOCKS_PER_SEC);
     printf("Convergence reached in %i steps",steps);
     std::cout<<"\nOutput:"<<std::endl;
     std::cout<<network->getNeuronStates()<<std::endl;
     network.reset();
}
*/

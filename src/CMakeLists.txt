
# Boost
find_package(Boost REQUIRED)
include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIR})

# Poco
find_library(POCO_FOUNDATION PocoFoundation REQUIRED)
find_library(POCO_NET PocoNet)
find_library(POCO_UTIL PocoUtil)

#Armadillo
find_package(Armadillo REQUIRED)
include_directories(${ARMADILLO_INCLUDE_DIRS})
link_directories(${ARMADILLO_LIBRARY_DIR})

# Neural
add_library(neural Neuron.cpp NeuronFactory.cpp Network.cpp)
target_link_libraries( neural  ${Boost_LIBRARIES} ${POCO_FOUNDATION} ${POCO_NET} ${POCO_UTIL} ${ARMADILLO_LIBRARIES})



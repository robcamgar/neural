/*
 * Neuron.cpp
 *
 *  Created on: 17/01/2013
 *      Author: Roberto de la Camara
 *      e-mail: roberto.de.la.camara.garcia@gmail.com
 */

#include <cstdio>
#include "Neuron.h"
#include <Poco/Exception.h>
#include <armadillo>


using namespace arma;


namespace neuron {


Neuron::Neuron() {
	state=0;
    internalState=0;
}

Neuron::Neuron(const Neuron& neuron) {

	state=neuron.state;
	internalState=neuron.internalState;
}

Neuron& Neuron::operator = ( const Neuron& neuron ) {

	state=neuron.state;
	internalState=neuron.internalState;
}


Neuron::~Neuron() {

}

void Neuron::update(const arma::vec           connectionWeights,
		             const arma::uvec           activationStates,
		             const double              bias)
{

	try {

		if(!connectionWeights.is_empty() && !activationStates.is_empty()) {


			unsigned int connectionWeightsSize=connectionWeights.size();
			unsigned int activationStatesSize=activationStates.size();
			poco_assert(connectionWeightsSize == activationStatesSize);

			printf("update-1");

			internalState=accu(connectionWeights%activationStates);

			printf("update-2");

			if(internalState!=0)
			{
				state=activationFunction(internalState);
				internalState=state;
			}
		}
		else {
			throw Poco::ApplicationException("Connection weights and activation states vectors must be non-empty");
		}
	}

	catch (Poco::Exception& exc) {
		std::cerr << exc.displayText() << std::endl;
	}

}


int Neuron::activationFunction(double internalState) {

	if(internalState>0)
			state=1;
	else if(internalState<0)
			state=-1;

	return state;
}


int Neuron::getState() {
	return state;
}

} /* namespace neuron */

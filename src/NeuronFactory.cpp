/*
 * NeuronFactory.cpp
 *
 *  Created on: 02/06/2013
 *      Author: Roberto de la Camara
 *      e-mail: roberto.de.la.camara.garcia@gmail.com
 */


#include "NeuronFactory.h"


neuron::NeuronFactory* neuron::NeuronFactory::instance=0L;

namespace neuron {

NeuronFactory* NeuronFactory::getInstance(){
		if(!instance){
			instance=new NeuronFactory;
		}
		return instance;
}

} /* namespace neuron */

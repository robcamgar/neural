/*
 * NeuronFactory.h
 *
 *  Created on: 02/06/2013
 *      Author: Roberto de la Cámara
 */

#ifndef NEURONFACTORY_H_
#define NEURONFACTORY_H_

#include "NeuronInterface.h"
#include "Neuron.h"
#include <Poco/DynamicFactory.h>
#include <Poco/SharedPtr.h>

using Poco::DynamicFactory;
using Poco::SharedPtr;


namespace neuron {

class NeuronFactory {
private:
	static NeuronFactory* instance;
	NeuronFactory() {};
	NeuronFactory(NeuronFactory const&);  // Don't Implement
	void operator=(NeuronFactory const&); // Don't implement

public:
	SharedPtr<NeuronInterface> getNeuron(const std::string neuronClass) {
		DynamicFactory<NeuronInterface> factory;
		factory.registerClass<Neuron>("DHNeuron");
		SharedPtr<NeuronInterface> neuron=factory.createInstance(neuronClass);
		return neuron;
	}
	static NeuronFactory* getInstance();
};

} /* namespace neuron */
#endif /* NEURONFACTORY_H_ */



/*
 * NeuronInterface.h
 *
 *  Created on: 02/06/2013
 *      Author: Roberto de la Cámara
 */

#ifndef NEURONINTERFACE_H_
#define NEURONINTERFACE_H_
#include <vector>
#include <armadillo>

using namespace std;

namespace neuron {

class NeuronInterface {
public:
	virtual ~NeuronInterface() {}
	/**
	 * Updates neuron activation state.
	 * \param connectionWeights Incoming connections weights from other neurons in the network
	 * \param activationStates Activation states of the rest of neurons of the network
	 * \param bias External input. Optional parameter.
     */
	 virtual void update( const arma::vec  connectionWeights,
		    		        const arma::uvec activationStates,
		   	    		    const double bias=0)=0;
	 /**
	  * Returns current activation state.
	  * \return state
	  */
	 virtual int getState()=0;
};

} /* namespace neuron */
#endif /* NEURONINTERFACE_H_ */

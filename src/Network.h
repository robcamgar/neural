/*
 * Network.h
 *
 *  Created on: 17/01/2013
 *      Author: Roberto de la Camara
 */


/**
 * Network class models a 1-layer neural network.
 *
 * \author Roberto de la Camara. roberto.de.la.camara.garcia@gmail.com
 */
#ifndef NETWORK_H_
#define NETWORK_H_

#include "NeuronInterface.h"
#include "NetworkInterface.h"
#include <Poco/SharedPtr.h>
#include <armadillo>

using namespace neuron;
using namespace std;
using namespace arma;
using Poco::SharedPtr;

namespace network {

class Network: public virtual NetworkInterface {

protected:
	/**
     * Number of neurons in the network.
     */
	unsigned int networkSize;
	/**
	 * Network's Neurons Activation States vector.
	 */
	arma::uvec neuronStates;
	/**
     * Network's Neurons vector.
	 */
	std::vector<SharedPtr<NeuronInterface> > neurons;
	/**
	 * Network's connection weights matrix.
	 */
	arma::mat connectionMatrix;
	/**
	 * Prints connection weights matrix
	 */
	void printConnectionMatrix();
	/**
	 * Initializes network.
	 * Creates neurons.
	 * Initializes connection matrix, neuron states and neurons.
	 */
	virtual void init();

public:
	/**
	 * Default constructor.
	 */
    Network(){};

    /**
     * Constructor.
     * Sets network size and calls init().
     * \param size. Network size
     */
	Network(const unsigned int size);
	/**
	 * Deletes neurons and then deletes network.
	 */
	virtual ~Network();

	/**
	 * Copy constructor.
	 */
	Network(const Network& network);
	/**
	 * Assignment operator.
	 */
	Network& operator = ( const Network& network );

	/**
	 * Updates network state.
	 * Calculates each neuron internal state taking into account connection weights and
	 * activation states of the rest of neurons.
	 * Then, applies activation function to each internal state to calculate next activation state.
	 */
	void update();
	/**
	 * Loads neurons initial state.
	 * \param initialStates
	 */
	void loadInitialState(arma::uvec initialStates);
	/**
	 * Loads weight matrix.
	 * \param connectionMatrix Neuron connection weight matrix.
	 */
	void loadConnectionMatrix(arma::mat connectionMatrix);
	/**
	 * Gets vector containing network states
	 * \return neuron activation states vector
	 */
    arma::uvec getNeuronStates();



};

} /* namespace network */
#endif /* NETWORK_H_ */

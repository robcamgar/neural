/*
 * Network.cpp
 *
 *  Created on: 17/01/2013
 *      Author: Roberto de la Camara
 */

#include "Network.h"
#include "NeuronFactory.h"
#include <cassert>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <stdio.h>

using namespace neuron;
using namespace boost::numeric::ublas;
using Poco::SharedPtr;
using Poco::DynamicFactory;

namespace network {

Network::Network(const unsigned int size) {

	assert(size>1);
	networkSize=size;
	init();

}

Network::Network(const Network& network) {

	assert(network.networkSize>1);
	networkSize=network.networkSize;
	neuronStates=network.neuronStates;
	connectionMatrix=network.connectionMatrix;
	neurons=network.neurons;

}

Network& Network::operator = ( const Network& network ) {

	assert(network.networkSize>1);
	networkSize=network.networkSize;
	neuronStates=network.neuronStates;
	connectionMatrix=network.connectionMatrix;
	neurons=network.neurons;

}

void Network::init() {

	neuronStates=zeros<uvec>(networkSize);
	connectionMatrix=zeros<mat>(networkSize,networkSize);
	neurons=std::vector<SharedPtr<NeuronInterface> >(networkSize);

	for (unsigned int i=0;i<networkSize;++i) {

		    SharedPtr<NeuronInterface> neuron=NeuronFactory::getInstance()->getNeuron("DHNeuron");
		    neurons[i]=neuron;
	}

}

Network::~Network() {

}



void Network::printConnectionMatrix()
{
	std::cout << connectionMatrix << std::endl;
}


void Network::update() {

	printf("hola-1");

	for(unsigned int i=0; i<networkSize;i++)
	{
		arma::rowvec incomingConnections=connectionMatrix.row(i);
		printf("hola-2");
	    neurons[i]->update(incomingConnections,neuronStates);
	    printf("hola-3");
    }

	printf("hola-2");

	for(unsigned int i=0; i<networkSize;i++)
	{
	   neuronStates[i]=neurons[i]->getState();

	}

}

void Network::loadInitialState(arma::uvec initialStates) {

	assert(initialStates.size()==networkSize);
	neuronStates=initialStates;
}


void Network::loadConnectionMatrix(arma::mat initialConnectionMatrix) {

	assert(initialConnectionMatrix.n_rows==initialConnectionMatrix.n_cols);
	assert(initialConnectionMatrix.n_cols==networkSize);
	connectionMatrix=initialConnectionMatrix;

}

arma::uvec Network::getNeuronStates() {

	return neuronStates;
}

} /* namespace network */

/*
 * NetworkInterface.h
 *
 *  Created on: 02/06/2013
 *      Author: Roberto de la Cámara
 */

#ifndef NETWORKINTERFACE_H_
#define NETWORKINTERFACE_H_

#include <vector>
#include <armadillo>

using namespace std;


namespace network {

class NetworkInterface {
public:
	     virtual ~NetworkInterface() {}
	    /**
		 * Updates network state.
		 * Calculates each neuron internal state taking into account connection weights and
		 * activation states of the rest of neurons.
		 * Then, applies activation function to each internal state to calculate next activation state.
		 */
		virtual void update()=0;
		/**
		 * Loads neurons initial state.
		 * \param initialStates
		 */
		virtual void loadInitialState(arma::uvec initialStates)=0;
		/**
		 * Loads weight matrix.
		 * \param connectionMatrix Neuron connection weight matrix.
		 */
		virtual void loadConnectionMatrix(arma::mat connectionMatrix)=0;
		/**
		 * Gets vector containing network states
		 * \return neuron activation states vector
		 */
	    virtual arma::uvec getNeuronStates()=0;

};

} /* namespace network */
#endif /* NETWORKINTERFACE_H_ */

/*
 * Neuron.h
 *
 *  Created on: 17/01/2013
 *      Author: Roberto de la C�mara
 *      e-mail: roberto.de.la.camara.garcia@gmail.com
 */

/**
 * Neuron class models a neuron.
 *
 * \author Roberto de la Cámara
 */

#ifndef NEURON_H_
#define NEURON_H_

#include <iostream>
#include "NeuronInterface.h"

using namespace std;

namespace neuron {

class Neuron: public virtual NeuronInterface {

protected:

	/**
	 * Activation State.
	 * Values: -1, +1
	 */
	int state;
	/**
	 * Internal value.
	 */
	double internalState;
	/**
	 * Activation Function.
	 * \param internalState
	 */
	int activationFunction(double internalState);

public:

	Neuron();

	Neuron(const Neuron& neuron);

	Neuron& operator = ( const Neuron& neuron );


	virtual ~Neuron();


	/**
     * Updates neuron activation state.
     * \param connectionWeights Incoming connections weights from other neurons in the network
     * \param activationStates Activation states of the rest of neurons of the network
     * \param bias External input. Optional parameter.
	 */
    void update(const arma::vec                    connectionWeights,
    		     const arma::uvec                   activationStates,
    		     const double bias=0);
    /**
     * Returns current activation state.
     * \return state
     */
    int getState();


};

} /* namespace neuron */
#endif /* NEURON_H_ */
